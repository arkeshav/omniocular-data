# Omniocular Data

Clone this repository to the folder that contains the [`omniocular`](https://github.com/omniocular/omniocular) codebase:

```bash
$ git clone https://github.com/omniocular/omniocular.git
$ git clone https://git.uwaterloo.ca/arkeshav/omniocular-data.git
```

After cloning the repository, that you have the following directory structure:

```
.
├── omniocular
└── omniocular-data
    ├── embeddings
    └── datasets
```

Additional datasets you wish to include should be placed in the `omniocular-data/datasets` folder. 
Finally, check if you have a text file containing the embeddings for Java:

```bash
cd omniocular-data/embeddings/
ls java1k_size300_min10.bin.txt 
```
